#!/bin/bash
echo "update 2"

NETPLAN=
HOSTNAME="cloudstack-001"
HOST_IP="192.168.1.11"
HOST_MASK="255.255.255.0"
DNS1="8.8.8.8"
DNS2="8.4.4.8"


apt_install() {
apt-get -y install openntpd openssh-server sudo vim htop tar
apt-get -y install intel-microcode
apt-get -y install bridge-utils
}

  ##  create netplan
create_netplan() {
cat > /etc/netplan/01-netconfig.yml << EOF
  network:
    version: 2
    renderer: networkd
    ethernets:
      eno1:
        dhcp4: false
        dhcp6: false
        optional: true
    bridges:
      cloudbr0:
        addresses: [192.168.1.10/24]
        routes:
         - to: default
           via: 192.168.1.1
        nameservers:
          addresses: [1.1.1.1,8.8.8.8]
        interfaces: [eno1]
        dhcp4: false
        dhcp6: false
        parameters:
          stp: false
          forward-delay: 0
EOF
}

create_netplan
